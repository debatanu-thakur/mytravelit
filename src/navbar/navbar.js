import * as html from "./navbar.html";
import $ from 'jquery';

$(function(){
// mobile menu slide from the left
    $('[data-toggle="slide-collapse"]').on('click', function(evt) {
        evt.preventDefault();
        window.$navMenuCont = $($(this).data('target'));
        window.$navMenuCont.animate({'width':'toggle'}, 280);
        $('body').on('click', slideIn);
    });
    
    function slideIn(evt) {
        console.log(evt.target);
        if(evt.target != window.$navMenuCont[0] && 
        evt.target != $('.navbar-toggler-icon')[0] && 
        !window.$navMenuCont.find($(evt.target)).length && 
        !$('[data-toggle="slide-collapse"]').find($(evt.target)).length) {
            window.$navMenuCont.animate({'width':'toggle'}, 280);
            $('body').off('click');
        }
        
    }
});
export default {
    navbarHTML : html.toString()
};