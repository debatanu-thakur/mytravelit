import $ from 'jquery';
import * as popper from 'popper.js';
import * as bootstrap from 'bootstrap';
import './styles.scss';

import router from './router.js';

import {map, api} from './core/core';

import navbar from './navbar/navbar.js';
import { mapNavHTML, callMapAPI } from './map-nav/map-nav';
import infoWinCont from './info-window-content/info-window-content';

router.on(() => {
    $('#view').html('');
    $('#navbar').html(navbar.navbarHTML);
    $('#view').html(mapNavHTML);
});
router.resolve();
callMapAPI();