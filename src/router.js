import * as Navigo from 'navigo';
var root = null;
var useHash = true; // Defaults to: false
var hash = '#!'; // Defaults to: '#'
const router = new Navigo(root, useHash, hash);

export default router;