import * as mapNav from './map-nav.html';
import $ from 'jquery';
import {map, api} from '../core/core';
import infoWinCont from '../info-window-content/info-window-content';

export const mapNavHTML = mapNav;
export function callMapAPI(){
    infoWinCont.core = {map, api};
    $.getScript('//www.google.com/jsapi', function()
    {
        google.load('maps', '3', {
            other_params: `key=AIzaSyC2Xryh23SOWVxOqNWKPDANkB9SQpPGe00&libraries=places`,
            callback: function()
            {
                const element = document.getElementById('map');

                infoWinCont.INIT(element);
            }
        });
    }).fail(() => alert('Loading failed for map API. Please try again.'));
}
