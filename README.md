# My Travel Itinerary
## Description
This app is your travle partner. This helps to plan your itinerary for different places and local events happening in all those places. This works on all kinds of devices mobile, tabs and desktops.
## Pre-requisites
1. Any HTML 5 supported browser, Chrome, Safari, Firefox.
2. NodeJS and npm installed globally.
3. Yarn package installer installed globally.

To install yarn globally 
```
$ npm install yarn -g
```

## Setup
Please follow the steps to load the application and run it in your browser:


1. `git clone` the solution to your local machine. 
2. Once inside the repo directory `yarn` and install all the dependency packages.
3. Then `npm run build` for starting the dev environment.
The site is now running in `http://localhost:8080`. Please make sure that the port is not used by anything else.
4. For production deployment, run `npm run prod`. All the files are now in `dist` folder and are ready to be hosted.
```
$ git clone git@bitbucket.org:debatanu-thakur/mytravelit.git
$ cd mytravelit
$ yarn
$ npm run build
```

## Technologies and APIs
1. This project is completed using `ECMASCRIPT 2015`.
2. The build tool used for this project is `Webpack 2`.
3. API used for map is [Google Map API](https://developers.google.com/maps/documentation/javascript/).
4. API used to fetch neighbouring venues is [Foursquare API](https://developer.foursquare.com/).
5. API used for images is [Google Street View API](https://developers.google.com/maps/documentation/streetview/).
6. API used for article in info window is [Wikipedia API](http://www.mediawiki.org/wiki/API:Main_page).

## License
MIT License

Copyright (c) 2018 Debatanu Thakur

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.